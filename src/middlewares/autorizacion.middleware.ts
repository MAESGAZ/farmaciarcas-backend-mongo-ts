/**
 * Importamos las librerías
 */
import { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';


import Usuario from '../models/usuario.model';
import { IPayLoad } from '../interfaces/payload.interface';
import { IUsuario } from '../interfaces/usuario.interface';
import { Exception } from '../error/exception.error';


export class Autorizacion {
    constructor() { }

    /**
     * VerificarToken
     * Verifica que el usuario tiene token
     * @param {Request} request
     * @param {Response} response
     * @returns response
     * @throws Exception
     */
    public async verificarToken(request: Request, response: Response, next: NextFunction): Promise<Response | void> {
        const token: string = request.header('Authorization') as string;

        try {
            if (!token)
                throw new Exception(0o2, 'Error', 'Se necesita la entrega del tokens');

            const payLoad: IPayLoad = jwt.verify(token, process.env.SECRET || "") as IPayLoad;
            const user: IUsuario = await Usuario.findById(payLoad._id, { password: 0 }) as IUsuario;

            if (!user)
                throw new Exception(0o1, 'Error', 'El usuario no se encuentra en Base de Datos')

            next();
        } catch (error) {
            return response.status(400).json({
                error
            })
        }
    }
}