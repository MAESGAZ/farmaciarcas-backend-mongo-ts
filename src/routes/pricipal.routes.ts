/**
 * Importamos las librerías
 */
import { Router } from 'express';

/**
 * Importamos las clases de la aplicación
 */
import { PrincipalController } from '../controllers/principal.controller';

/**
 * Creamos la clase Principal
 */
export class Principal {
    private principal: Router;

    constructor() {
        this.principal = Router();
        this.rutas();
    }

    private rutas(): void {
        this.principal.get(String(process.env.PRINCIPAL), new PrincipalController().getPrincipal)
            .post(String(process.env.PRINCIPAL), new PrincipalController().getPrincipal)
            .put(String(process.env.PRINCIPAL), new PrincipalController().getPrincipal)
            .delete(String(process.env.PRINCIPAL), new PrincipalController().getPrincipal)
    }

    public getPrincipal(): Router {
        return this.principal;
    }
}