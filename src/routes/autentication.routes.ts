import { Router } from 'express';

import { Autenticacion } from '../controllers/autenticacion.controller';

export class AutenticacionRoutes {
    private authRutas: Router;

    constructor() {
        this.authRutas = Router();
        this.rutas();
    }

    private rutas(): void {
        this.authRutas.post('/login', new Autenticacion().login)
            .post('/registro', new Autenticacion().registrar)
    }

    public getRutas(): Router {
        return this.authRutas;
    }
}