import { Router } from 'express';
import { ClienteController } from '../controllers/cliente.controller';



export class ClienteRoutes {
    private cliRutas: Router;

    constructor() {
        this.cliRutas = Router();
        this.rutas();
    }

    private rutas(): void {
        this.cliRutas.get('/', new ClienteController().getClientes)
        .get(process.env.ID ||'/:id', new ClienteController().getCliente)
        .post('/', new ClienteController().newCliente)
        .put(process.env.ID ||'/:id', new ClienteController().updateCliente)
        .delete(process.env.ID ||'/:id', new ClienteController().deleteCliente)
    }

    public getRutas(): Router {
        return this.cliRutas;
    }
}