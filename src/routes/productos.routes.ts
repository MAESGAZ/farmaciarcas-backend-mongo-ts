import { Router } from 'express';

import { ProductosController } from '../controllers/productos.controller';

export class ProductosRutas {
    private producRutas: Router;

    constructor() {
        this.producRutas = Router();
        this.rutas();
    }

    private rutas(): void {
        this.producRutas.get('/', new ProductosController().getProductos)
        .get('/:id', new ProductosController().getProducto)
    }

    public getRutas(): Router {
        return this.producRutas;
    }
}