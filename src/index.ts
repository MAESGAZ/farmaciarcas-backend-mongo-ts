/**
 * Importaciones de librerías
 */
import { config } from 'dotenv';

/**
 * Importamos las clases de la aplicación
 */
import { Servidor } from './server';

/**
 * Iniciamos las variables de entorno
 */
config();

/**
 * Instanciamos el servidor y lo iniciamos.
 */
new Servidor(Number(process.env.PORT)).iniciarServidor();