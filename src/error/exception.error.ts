export class Exception extends Error {
    private id: string;
    private error: string;
    private descripcion: string;

    constructor(id: number, error: string, descripcion: string) {
        super();
        this.id = `SERV-${id}`;
        this.error = error;
        this.descripcion = descripcion;
    }

    public setId(id: string): void {
        this.id = `SERV-${id}`;
    }

    public getId(): string {
        return this.id;
    }

    public setError(error: string): void {
        this.error = error;
    }

    public getError(): string {
        return this.error;
    }

    public setDescripcion(descripcion: string): void {
        this.descripcion = descripcion;
    }

    public getDescripcion(): string {
        return this.descripcion;
    }
}