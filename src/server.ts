/**
 * Importaciones de librerías
 */
import express, { Application } from 'express';
import cors from 'cors';
import helmet from 'helmet';
import compression from 'compression';
import morgan from 'morgan';

/**
 * Importaciones de clases del programa
 */
import { Principal } from './routes/pricipal.routes';
import { Database } from './database/database';
import { AutenticacionRoutes } from './routes/autentication.routes';
import { Autorizacion } from './middlewares/autorizacion.middleware';
import { ProductosRutas } from './routes/productos.routes';
import { ClienteRoutes } from './routes/cliente.routes';

/**
 * Clase Servidor
 */
export class Servidor {
    private servidor: Application;

    constructor(puerto: Number) {
        this.servidor = express();
        this.middlewares();
        this.configurarPuerto(puerto);
        new Database();
        this.rutas();
    }

    private middlewares(): void {
        this.servidor
            .use(express.json())
            .use(express.urlencoded({ extended: false }))
            .use(morgan('dev'))
            .use(cors())
            .use(helmet())
            .use(compression())
    }

    private configurarPuerto(puerto: Number): void {
        this.servidor.set('port', puerto || process.env.PORT);
    }

    private rutas(): void {
        this.servidor.use(String(process.env.NOMBRE), new Principal().getPrincipal())
            .use(process.env.NOMBRE || "/autentication", new AutenticacionRoutes().getRutas())
            .use(`${process.env.NOMBRE}${process.env.PRODUCTO}` || "/producto", new Autorizacion().verificarToken,new ProductosRutas().getRutas())
            .use(`${process.env.NOMBRE}${process.env.CLIENTE}`, new Autorizacion().verificarToken, new ClienteRoutes().getRutas()) 
    }

    public iniciarServidor(): void {
        this.servidor.listen(this.servidor.get('port'), () => console.log(`Servidor en el puerto ${this.servidor.get('port')}`));
    }
}