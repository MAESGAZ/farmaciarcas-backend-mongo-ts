import { Document } from 'mongoose';

export interface ICliente extends Document {
    _id: string;
    nombre: string;
    apellido1: string;
    apellido2: string;
    fecha_nacimiento: Date;
    dni: string;
    num_afi_ss: number;
    direccion: string;
    email: string;
    telefono: number;
    fecha_creacion: Date;
}