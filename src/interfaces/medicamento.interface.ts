import { Document } from 'mongoose';

export interface IMedicamento extends Document {
    _id: string;
    nombre: string;
    descripcion: string;
    restricciones_medicamento: string;
    precio_abono: number;
    precio_pvp: number;
    cantidad: number;
    fecha_creacion: Date;
}