import { Document } from 'mongoose';

export interface IProductos extends Document {
    _id: string;
    nombre: string;
    descripcion: string;
    precio: number;
    cantidad: number;
    fecha_creacion: Date;
}