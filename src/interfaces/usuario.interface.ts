import { Document } from 'mongoose';

export interface IUsuario extends Document {
    nombre: string;
    apellido1: string;
    apellido2: string;
    username: string;
    email: string
    password: string;
    fecha_registro: Date;
    encryptedPassword(password: string): Promise<string>;
    comparePassword(password: string, receivedPassword: string): Promise<boolean>;
}