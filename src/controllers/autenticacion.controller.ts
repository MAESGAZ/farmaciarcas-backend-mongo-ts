import { Request, Response } from 'express';
import { IUsuario } from '../interfaces/usuario.interface';
import jwt from 'jsonwebtoken';

import Usuario from '../models/usuario.model';


export class Autenticacion {
    constructor() {}

    public async login(request: Request, response: Response): Promise<Response> {
        const username: string = request.body.username;
        const password: string = request.body.password;

        if (!(username && password)) {
            return response.status(400).json({
                message: `El usuario o la contraseña no se han introducido`
            });
        }

        const usuario: IUsuario = await Usuario.findOne({ username: username }) as IUsuario;

        if (!usuario) return response.status(401).json({
            error: 'El usuario no existe en BBDD'
        });

        if (!usuario.comparePassword(usuario.password, password)) return response.status(402).json({ error: 'Contraseña incorrecta' });

        const token: string = jwt.sign({ _id: usuario._id }, process.env.SECRET || 'FarmaciArcasAPI', {
            expiresIn: 60 * 60 * 24
        });

        return response.status(200).json({
            username: usuario.username,
            nombre: usuario.nombre,
            token: token
        });
        
    }

    public async registrar(request: Request, response: Response): Promise<Response> {
        const nombre: string = request.body.nombre;
        const apellido1: string = request.body.apellido1;
        const apellido2: string = request.body.apellido2;
        const username: string = request.body.username;
        const email: string = request.body.email;
        const password: string = request.body.password;

        if (!(nombre && apellido1 && apellido2 && username && password))
        return response.status(400).json({
            error: 'Campos incompletos, revise la información introducida'
        });

        const usuario: IUsuario = new Usuario({
            nombre: nombre,
            apellido1: apellido1,
            apellido2: apellido2,
            username: username,
            email: email
        });
        usuario.password = await usuario.encryptedPassword(password);

        const savedUsuario: IUsuario = await usuario.save();

        if (!savedUsuario)
        return response.status(402).json({
            error: 'No se ha podido guardar el usuario'
        });

        const token = jwt.sign({ _id: savedUsuario._id }, process.env.SECRET || 'FarmaciArcasAPI', {
            expiresIn: 60*60*24
        })

        return response.status(200).json({
            token: token
        });
    }
}