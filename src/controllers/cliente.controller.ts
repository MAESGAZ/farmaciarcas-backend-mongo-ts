import { Request, Response } from 'express';
import { Exception } from '../error/exception.error';

import { ICliente } from '../interfaces/cliente.interface';
import Cliente from '../models/cliente.model';

export class ClienteController {
    constructor() {}

    public async getClientes(request: Request, response: Response): Promise<Response> {
        try {
            const clientes: ICliente[] = await Cliente.find();

            return response.status(200).json({
                response: clientes
            });
        } catch (error) {
            return response.json(400).json({
                error: new Exception(1, 'Error durante la busqueda', error)                
            });
        }
        
        
    }

    public async getCliente(request: Request, response: Response): Promise<Response> {
        try {
            const id: string = request.params.id;
            if (!id) throw new Exception(12, 'Error en los parámetros', process.env.ID_EXCEP || "");

            const cliente: ICliente = await Cliente.findById(id) as ICliente;

            return response.status(200).json({
                response: cliente
            });
        } catch (error) {
            return response.status(400).json({
                error: error
            })
        }
        
    }

    public async newCliente(request: Request, response: Response): Promise<Response> {
        try {
            const nombre: string = request.body.nombre;
            const apellido1: string = request.body.apellido1;
            const apellido2: string = request.body.apellido2;
            const fecha_nacimiento: Date = request.body.fecha_nacimiento;
            const dni: string = request.body.dni;
            const num_afi_ss: number = request.body.num_afi_ss;
            const direccion: string = request.body.direccion;
            const email: string = request.body.email;
            const telefono: number = request.body.telefono;

            if (!(nombre && apellido1 && apellido2 && dni && num_afi_ss && direccion && email && telefono))
                throw new Exception(13, 'Parámetros vacíos', process.env.PARAM_EXCEP || "");

            const cliente: ICliente = new Cliente({
                nombre: nombre,
                apellido1: apellido1,
                apellido2: apellido2,
                fecha_nacimiento: fecha_nacimiento,
                dni: dni,
                num_afi_ss: num_afi_ss,
                direccion: direccion,
                email: email,
                telefono: telefono,
                fecha_creacion: new Date()
            });

            const gCliente: ICliente = await cliente.save();

            if (!gCliente)
                throw new Exception(14, 'Error al insertar', process.env.GUA_EXCEP || "");
            
            return response.status(200).json({
                response: gCliente
            });
        } catch (error) {
            return response.status(400).json({
                error: error
            })
        }
    }

    public async deleteCliente(request: Request, response: Response): Promise<Response> {
        try {
            const id: string = request.params.id;
            if (!id) throw new Exception(12, 'Error en los parámetros', process.env.ID_EXCEP || "");

            const deleteCliente: ICliente | null = await Cliente.findByIdAndDelete(id);
            if (!deleteCliente) 
                throw new Exception(15, 'Error al realizar la busqueda', 'El usuario no existe en base de datos')
            return response.status(200).json({
                response: 'Usuario eliminado'
            })
        } catch (error) {
            return response.status(400).json({
                error
            })
        }
    }

    public async updateCliente(request: Request, response: Response): Promise<Response> {
        try {
            
            return response.status(200).json({});
        } catch (error) {
            return response.status(400).json({
                error: error
            })
        }
    }
}