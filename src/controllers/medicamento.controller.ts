import { Request, Response } from 'express';

import Medicamento from '../models/medicamento.model';
import { IMedicamento } from '../interfaces/medicamento.interface';
import { Exception } from '../error/exception.error'

export class MedicamentoController {
    constructor() {}

    public async getMedicamentos(request: Request, response: Response): Promise<Response> {
        try {
            const medicamentos: IMedicamento[] = await Medicamento.find();
            return response.status(200).json({
                response: medicamentos
            });
        } catch (error) {
            return response.status(400).json({
                error: error
            })
        }
    }

    public async getMedicamento(request: Request, response: Response): Promise<Response> {
        const id: string = request.params.id;

        try {
            if (!id) throw new Exception(12, 'Error en los parámetros', process.env.ID_EXCEP || "");

            const medicamento: IMedicamento = await Medicamento.findById(id) as IMedicamento;

            return response.status(200).json({
                response: medicamento
            });
        } catch (error) {
            return response.status(400).json({
                error
            });
        }
    }
}