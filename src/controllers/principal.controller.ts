/**
 * Importamos las librerías
 */
import { Request, Response } from 'express';

/**
 * Importamos las clases necesarias para la aplicación
 */
import info from '../../package.json'

/**
 * Creamos la clase PrincipalController
 */
export class PrincipalController {
    
    constructor() {}

    public async getPrincipal(request: Request, response: Response): Promise<void> {
        response.status(200).json({
            name: info.name,
            description: info.description,
            version: info.version,
            routes: [

            ],
            protocols: [
                "PUT",
                "GET",
                "POST",
                "DELETE"
            ],
            protected_routes: true
        })
    }
}