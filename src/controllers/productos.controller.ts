import { Request, Response } from 'express';

import Producto from '../models/producto.model';
import { IProductos } from '../interfaces/productos.interface';

export class ProductosController {
    constructor() { }

    public async getProductos(request: Request, response: Response): Promise<Response> {
        const productos: IProductos[] = await Producto.find();

        if (!productos)
            return response.status(400).json({
                error: 'No existen datos en la tabla Productos'
            });

        return response.status(200).json({
            response: productos
        });
    }

    public async getProducto(request: Request, response: Response): Promise<Response> {
        const id: string = request.params.id;

        const producto: IProductos = await Producto.findById({ _id: id }) as IProductos;

        if (!producto)
        return response.status(200).json({
            error: 'No se han encontrado ningún producto mediante el id'
        });

        return response.status(200).json({
            response: producto
        });
    }
}