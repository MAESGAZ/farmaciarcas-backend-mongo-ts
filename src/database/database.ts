/**
 * Importamos las librerías
 */
import mongoose from 'mongoose';

/**
 * Creamos las clases
 */

export class Database {

    constructor() {
        mongoose.connect("mongodb://localhost/farmaciarcas", {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: true,
            useCreateIndex: true
        })
            .then(db => console.log('Conectado a la BBDD'))
            .catch(error => console.log(error));
    }
}