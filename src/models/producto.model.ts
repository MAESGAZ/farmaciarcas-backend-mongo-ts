import { Schema, model } from 'mongoose';

import { IProductos } from '../interfaces/productos.interface';

const productoSchema: Schema = new Schema({
    nombre: {
        type: String,
        required: true,
    },
    descripcion: {
        type: String,
        required: true
    },
    precio: {
        type: Number,
        required: true
    },
    cantidad: {
        type: Number,
        required: false
    },
    fecha_creacion: {
        type: Date,
        default: new Date()
    }
}, {
    timestamps: true
});


export default model<IProductos>('Producto', productoSchema);