import { Schema, model } from 'mongoose';

import { ICliente } from '../interfaces/cliente.interface';

const clienteSchema: Schema = new Schema({
    nombre: {
        type: String,
        required: true,
    },
    apellido1: {
        type: String,
        required: true,
    },
    apellido2: {
        type: String,
        required: true
    },
    fecha_nacimiento: {
        type: Date
    },
    dni: {
        type: String,
        required: true
    },
    num_afi_ss: {
        type: Number,
        required: true
    },
    direccion: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    telefono: {
        type: Number
    },
    fecha_creacion: {
        type: Date
    }
});

export default model<ICliente>('Cliente', clienteSchema);