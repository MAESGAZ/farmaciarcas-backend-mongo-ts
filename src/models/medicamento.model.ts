import { Schema, model } from 'mongoose';

import { IMedicamento } from '../interfaces/medicamento.interface';

const medicamentoSchema: Schema = new Schema({
    nombre: {
        type: String,
        required: true,
    },
    descripcion: {
        type: String
    },
    restricciones_medicamento: {
        type: String,
        required: true,
    },
    precio_abono: {
        type: Number,
        required: true
    },
    precio_pvp: {
        type: Number,
        required: true
    },
    cantidad: {
        type: Number
    },
    fecha_creacion: {
        type: Date
    }
});

export default model<IMedicamento>('Medicamento', medicamentoSchema);