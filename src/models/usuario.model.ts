import { Schema, model } from 'mongoose';
import bcrypt from 'bcrypt';

import { IUsuario } from '../interfaces/usuario.interface';

const usuarioSchema = new Schema({
    nombre: {
        type: String,
        required: true
    },
    apellido1: {
        type: String,
        required: true
    },
    apellido2: {
        type: String,
        required: true
    },
    username: {
        type: String,
        required: true,
        unique: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    fecha_registro: {
        type: Date,
        default: new Date()
    }
});

usuarioSchema.methods.encryptedPassword = async (password: string): Promise<string> => {
    const salt = await bcrypt.genSalt(10);
    return await bcrypt.hash(password, salt);
}

usuarioSchema.methods.comparePassword = async (password: string, receivedPassword: string): Promise<boolean> => {
    return await bcrypt.compare(receivedPassword, password);
}

export default model<IUsuario>('Usuario', usuarioSchema);